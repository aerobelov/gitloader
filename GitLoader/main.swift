//
//  main.swift
//  GitLoader
//
//  Created by Pavel Belov on 06.11.2019.
//  Copyright © 2019 Pavel Belov. All rights reserved.
//

import Foundation
import Alamofire

let group = DispatchGroup()
struct repo: Decodable {
    let name: String
}
struct errMsg: Decodable {
    let message: String
}
var repos: [repo]?
let decoder = JSONDecoder()
var jsonData: Data?
var msg: errMsg?

print("Input Github user and press enter\n")
if let input = readLine() {
    group.enter()
    AF.request("https://api.github.com/users/\(input)/repos").responseJSON { response in
        if let data = response.data {
            jsonData = data
        } else {
            print("Empty response")
            exit(0)
        }
        group.leave()
    }
}

group.notify(queue: DispatchQueue.main) {
    do {
        if jsonData != nil {
            //Try for entered user (Response is Array)
            do {
                if let repo_list = try decoder.decode([repo]?.self, from: jsonData!) {
                    repos = repo_list
                    print("List of repositories:\n")
                    if repos!.count != 0 {
                        for rep in repos! {
                            print(rep.name)
                        }
                    } else {
                        print("User doesn't have public repositories")
                    }
                    exit(0)
                }
            }
            //If user not found (Response is Dict)
            catch {
                if let msg = try decoder.decode(errMsg?.self, from: jsonData!) {
                    print(msg.message)
                exit(0)
                }
            }
            
        }
        
    } catch {
        print(error)
    }
}
dispatchMain()




